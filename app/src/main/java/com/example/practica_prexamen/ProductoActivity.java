package com.example.practica_prexamen;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import database.Producto;
import database.ProductosConsultas;

public class ProductoActivity extends AppCompatActivity {
    private EditText edtCodigo;
    private Button btnBuscar;
    private EditText edtNombre;
    private EditText edtMarca;
    private EditText edtPrecio;
    private RadioGroup radioGroup;
    private RadioButton rdbPerecedero;
    private RadioButton rdbNoPerecedero;
    private Button btnBorrar;
    private Button btnActualizar;
    private Button btnCerrar;
    private ProductosConsultas db;
    private Producto savedProduct;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        db = new ProductosConsultas(ProductoActivity.this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_producto);
        edtCodigo = (EditText) findViewById(R.id.edtCodigo);
        btnBuscar = (Button) findViewById(R.id.btnBuscar);
        edtNombre = (EditText) findViewById(R.id.edtNombre);
        edtMarca = (EditText) findViewById(R.id.edtMarca);
        edtPrecio = (EditText) findViewById(R.id.edtPrecio);
        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        rdbPerecedero = (RadioButton) findViewById(R.id.rdbPerecedero);
        rdbNoPerecedero = (RadioButton) findViewById(R.id.rdbNoPerecedero);
        btnBorrar = (Button) findViewById(R.id.btnBorrar);
        btnActualizar = (Button) findViewById(R.id.btnActualizar);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btnBorrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.openDataBase();
                db.deleteProducto(Integer.parseInt(edtCodigo.getText().toString()));
                Toast.makeText(ProductoActivity.this, "Se elimino el producto", Toast.LENGTH_LONG).show();
                db.cerrar();
                limpiar();

            }
        });
        btnActualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.openDataBase();
                if (!edtCodigo.getText().toString().matches("") ||
                        !edtNombre.getText().toString().matches(""))
                {
                    Producto producto = new Producto();
                    producto.setCodigo(savedProduct.getCodigo());
                    producto.setNombre(edtNombre.getText().toString());
                    producto.setPrecio(Float.parseFloat(edtPrecio.getText().toString()));
                    producto.setMarca(edtMarca.getText().toString());
                    producto.setPerecedero(rdbPerecedero.isChecked() ? 1:0);
                    if (db.updateProducto(producto,producto.getCodigo()) != -1)
                        Toast.makeText(ProductoActivity.this, "Se Actualizo el registro: " + producto.getCodigo(), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(ProductoActivity.this, "Se produjo un error", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(ProductoActivity.this, "Faltan campos",
                            Toast.LENGTH_SHORT).show();

                }
                limpiar();
                db.cerrar();


            }
        });
        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.openDataBase();
                Producto producto = db.getProducto(Integer.parseInt(edtCodigo.getText().toString()));
                if(producto != null)
                {
                    savedProduct = producto;
                    edtMarca.setText(producto.getMarca());
                    edtNombre.setText(producto.getNombre());
                    edtPrecio.setText(String.valueOf(producto.getPrecio()));
                    radioGroup.check(producto.getPerecedero()>0 ? R.id.rdbPerecedero : R.id.rdbNoPerecedero);
                }
                else
                {
                    Toast.makeText(ProductoActivity.this, "No se encontro producto", Toast.LENGTH_LONG).show();
                }
                db.cerrar();

            }
        });
    }

    private void limpiar()
    {
        edtCodigo.setText("");
        edtNombre.setText("");
        edtMarca.setText("");
        edtPrecio.setText("");
        rdbPerecedero.setChecked(true);
        savedProduct = null;

    }
}







