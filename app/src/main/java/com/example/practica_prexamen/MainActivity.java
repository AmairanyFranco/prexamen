package com.example.practica_prexamen;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import database.Producto;
import database.ProductosConsultas;


public class MainActivity extends AppCompatActivity {
    private EditText edtCodigo;
    private EditText edtNombre;
    private EditText edtMarca;
    private EditText edtPrecio;
    private RadioGroup radioGroup;
    private RadioButton rdbPerecedero;
    private RadioButton rdbNoPerecedero;
    private Button btnGuardar;
    private Button btnLimpiar;
    private Button btnNuevo;
    private Button btnEditar;
    private ProductosConsultas db;
    private Producto savedProduct;
    private int id;

    private void limpiar(){
        edtCodigo.setText("");
        edtNombre.setText("");
        edtPrecio.setText("");
        edtMarca.setText("");
        rdbPerecedero.setChecked(true);}
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtCodigo = (EditText) findViewById(R.id.edtCodigo);
        edtNombre = (EditText) findViewById(R.id.edtNombre);
        edtMarca = (EditText) findViewById(R.id.edtMarca);
        edtPrecio = (EditText) findViewById(R.id.edtPrecio);
        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        rdbPerecedero = (RadioButton) findViewById(R.id.rdbPerecedero);
        rdbNoPerecedero = (RadioButton) findViewById(R.id.rdbNoPerecedero);
        btnGuardar = (Button) findViewById(R.id.btnGuardar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnNuevo = (Button) findViewById(R.id.btnNuevo);
        btnEditar = (Button) findViewById(R.id.btnEditar);
        db = new ProductosConsultas(MainActivity.this);

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });
        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtCodigo.getText().toString().matches("") ||
                        edtNombre.getText().toString().matches("")) {
                    Toast.makeText(MainActivity.this, "Favor de llenar los campos", Toast.LENGTH_SHORT).show();
                } else {
                    Producto nProducto = new Producto();
                    nProducto.setNombre(edtNombre.getText().toString());
                    nProducto.setMarca(edtMarca.getText().toString());
                    nProducto.setPrecio(Float.parseFloat(edtPrecio.getText().toString()));
                    nProducto.setPerecedero(rdbPerecedero.isChecked() ? 1 : 0);
                    db.openDataBase();
                    if (savedProduct == null) {
                        long idx = db.insertarContacto(nProducto);
                        Toast.makeText(MainActivity.this, "Se agrego correctamente con ID: " + idx, Toast.LENGTH_SHORT).show();
                        limpiar();

                    } else {
                        db.UpdateContacto(nProducto, id);
                        Toast.makeText(MainActivity.this, "Se Actualizo el registro: " + id, Toast.LENGTH_SHORT).show();

                    }
                    db.cerrar();
                }

            }
        });
        btnEditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, ProductoActivity.class);
                startActivityForResult(i, 0);
            }
        });
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });
    }
    protected void onActivityResult(int requestCode,int resultCode,Intent data){
        if(Activity.RESULT_OK==resultCode){
            Producto producto = (Producto) data.getSerializableExtra("contacto");
            savedProduct = producto;
            id=producto.getCodigo();
            edtNombre.setText(savedProduct.getNombre());
            edtMarca.setText(savedProduct.getMarca());
            edtPrecio.setText(String.valueOf(savedProduct.getPrecio()));

            if(producto.getPerecedero()>0){
                rdbPerecedero.setChecked(true);}
            else{
                rdbNoPerecedero.setChecked(true);}

        }

    }

}
